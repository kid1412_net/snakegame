all: snakegame

snakegame: main.o snake.o
	g++ -lncurses -Wall main.o snake.o -o snakegame

main.o: main.cpp
	g++ -c -Wall main.cpp

snake.o: snake.cpp
	g++ -c -Wall snake.cpp

clean:
	rm -rf *o snakegame

