#include "snake.h"

using namespace std;

snakepart::snakepart(int col,int row)
{
	x=col;
	y=row;
}

snakepart::snakepart()
{
	x=0;
	y=0;
}

snakeclass::snakeclass()
{
	initscr();
	nodelay(stdscr,true);			//if there wasn't any key pressed don't wait for keypress
	keypad(stdscr,true);			//init the keyboard
	noecho();									//don't write
	curs_set(0);							//cursor invisible
	getmaxyx(stdscr,maxheight,maxwidth);

	partchar='x';
	oldalchar=(char)219;
	etel='*';
	SelChar = 254;

	food.x=0;
	food.y=0;

	for(int i=0;i<5;i++)
		snake.push_back(snakepart(40+i,10));

	points=0;
	del=110000;
	get=0;
	direction='l';
	exit_flag = false;
	theme_id = 0;
	speed_level = 1; // Medium speed

	srand(time(NULL));

	// putfood();
	draw_outline();

	// Check whether the  terminal supports color
	// if true => can choose options in theme menu
	// else => back to start menu
	start_color();
	// color_set(COLOR_PAIR(1))
	// refresh();

	init_pair(1, COLOR_BLUE, COLOR_BLACK)   ;
	init_pair(2, COLOR_YELLOW, COLOR_BLACK) ;
	init_pair(3, COLOR_YELLOW, COLOR_BLUE)  ;
	init_pair(4, COLOR_BLACK, COLOR_WHITE)  ;

	attron(COLOR_PAIR(3));
	bkgd(COLOR_PAIR(3));

	// attroff(COLOR_PAIR(1));
}

snakeclass::~snakeclass()
{
	nodelay(stdscr,false);			//turn back
	getch();										//wait until a key is pressed
	endwin();
}

void snakeclass::putfood()
{
	while(1)
	{
		int tmpx=rand()%maxwidth+1;
		int tmpy=rand()%maxheight+1;
		for(int i=0;i<snake.size();i++)
			if(snake[i].x==tmpx && snake[i].y==tmpy)
				continue;
		if(tmpx>=maxwidth-2 || tmpy>=maxheight-3)
			continue;
		food.x=tmpx;
		food.y=tmpy;
		break;
	}
	move(food.y,food.x);
	addch(etel);
	refresh();
}

bool snakeclass::collision()
{
	if(snake[0].x==0 || snake[0].x==maxwidth-1 || snake[0].y==0 || snake[0].y==maxheight-2)
		return true;
	for(int i=2;i<snake.size();i++)
	{
		if(snake[0].x==snake[i].x && snake[0].y==snake[i].y)
			return true;
	}
	//collision with the food
	if(snake[0].x==food.x && snake[0].y==food.y)
	{
		get=true;
		putfood();
		points+=10;
		move(maxheight-1,0);
		printw("%d",points);
		if((points%100)==0)
			del-=10000;
	}else
		get=false;
	return false;
}


void snakeclass::movesnake()
{
	//detect key
	int tmp=getch();
	switch(tmp)
	{
		case KEY_LEFT:
			if(direction!='r')
				direction='l';
			break;
		case KEY_UP:
			if(direction!='d')
				direction='u';
			break;
		case KEY_DOWN:
			if(direction!='u')
				direction='d';
			break;
		case KEY_RIGHT:
			if(direction!='l')
				direction='r';
			break;
		case KEY_BACKSPACE:
			direction='q';
			break;
	}
	//if there wasn't a collision with food
	if(!get)
	{
		move(snake[snake.size()-1].y,snake[snake.size()-1].x);
		printw(" ");
		refresh();
		snake.pop_back();
	}
	if(direction=='l')
	{
		snake.insert(snake.begin(),snakepart(snake[0].x-1,snake[0].y));
	}else if(direction=='r'){
		snake.insert(snake.begin(),snakepart(snake[0].x+1,snake[0].y));

	}else if(direction=='u'){
		snake.insert(snake.begin(),snakepart(snake[0].x,snake[0].y-1));
	}else if(direction=='d'){
		snake.insert(snake.begin(),snakepart(snake[0].x,snake[0].y+1));
	}
		move(snake[0].y,snake[0].x);
		addch(partchar);
	refresh();
}

void snakeclass::draw_outline()
{
	//make the game-board -- up-vertical
	for(int i=0;i<maxwidth-1;i++)
	{
		move(0,i);
		addch(oldalchar);
	}

	//left-horizontal
	for(int i=0;i<maxheight-1;i++)
	{
		move(i,0);
		addch(oldalchar);
	}

	//down-vertical
	for(int i=0;i<maxwidth-1;i++)
	{
		move(maxheight-2,i);
		addch(oldalchar);
	}

	//right-horizontal
	for(int i=0;i<maxheight-1;i++)
	{
		move(i,maxwidth-2);
		addch(oldalchar);
	}
}

void snakeclass::start()
{
	// Menu screen
	choose_menu();

	attroff(COLOR_PAIR(3));

	switch ( theme_id )
	{
		case 0:
			attron(COLOR_PAIR(1)) ;
			bkgd(COLOR_PAIR(1))   ;
			break                 ;

		case 1:
			attron(COLOR_PAIR(2)) ;
			bkgd(COLOR_PAIR(2))   ;
			break                 ;

		case 2:
			attron(COLOR_PAIR(3)) ;
			bkgd(COLOR_PAIR(3))   ;
			break                 ;

		case 3:
			attron(COLOR_PAIR(4)) ;
			bkgd(COLOR_PAIR(4))   ;
			break                 ;

		default:
			attron(COLOR_PAIR(1)) ;
			bkgd(COLOR_PAIR(1))   ;
			break                 ;
	}

	switch ( speed_level )
	{
		// Low speed
		case 0:
			del *= 1.5 ;
			break   ;

		// Medium speed
		case 1:
			break   ;

		// High speed
		case 2:
			del /= 1.5 ;
			break   ;

		default :
			break   ;
	}

	while(1)
	{
		if(collision() || exit_flag)
		{
			erase();
			draw_outline();
			move(12,36);
			printw("game_over");
			break;
		}
		movesnake();
		if(direction=='q')				//exit
			break;
		usleep(del);			//Linux delay
	}
}

void snakeclass::choose_menu()
{
	int CurPosSel = 0;
	int tmp = getch();
	bool is_theme_option;
	bool is_speed_option;

	is_theme_option = false;
	is_speed_option = false;

	// If input != enter && is_theme_option = true => continue
	while (1)
	{

		move(9, 36);
		printw("Start");

		move(10, 36);
		printw("Speed");

		move(11, 36);
		printw("Theme");

		move(12, 36);
		printw("Exit ");

		// move(9, 34);
		// addch(SelChar);

		tmp = getch();

		// Change select cursor position
		switch(tmp)
		{
			case KEY_DOWN:
				if ( CurPosSel < 3 )
					CurPosSel += 1;
				break;
			case KEY_UP:
				if ( CurPosSel > 0 )
					CurPosSel -= 1;
				break;
			default:
				break;
		}

		// Clear SelChar
		for ( int i=0; i < 4; ++i )
		{
			move(9+i, 34);
			printw(" ");
		}

		switch(CurPosSel)
		{
			// Start
			case 0:
				move(9, 34);
				addch(SelChar);
				exit_flag = false;
				is_theme_option = false;
				is_speed_option = false;
				break;

			// Speed
			case 1:
				move(10, 34);
				addch(SelChar);
				exit_flag = false;
				is_theme_option = false;
				is_speed_option = true ;
				break;

			// Theme
			case 2:
				move(11, 34);
				addch(SelChar);
				exit_flag = false;
				is_theme_option = true ;
				is_speed_option = false;
				break;

			// Exit
			case 3:
				move(12, 34);
				addch(SelChar);
				exit_flag 	    = true;
				is_theme_option = false;
				is_speed_option = false;
				break;

			// Default
			default:
				move(9, 34);
				addch(SelChar);
				exit_flag       = false;
				is_theme_option = false;
				break;
		}

		// For debugging
		// move(1, 1);
		// printw("Pos: %d", CurPosSel);
		// move(2, 1);
		// printw("theme_id: %d", theme_id);

		// bad coding style!!!
		if ( tmp != '\n' )
		{
			continue;
		}
		else if ( tmp == '\n' && is_theme_option )
		{
			choose_theme();
		}
		else if ( tmp == '\n' && is_speed_option )
		{
			choose_speed();
		}
		else if ( tmp == '\n' && !is_theme_option  )
		{
			break;
		}
		else
		{
			break;
		}

		refresh();
	}

	// Re-draw the outline
	erase();
	draw_outline();

	putfood();

	//draw the snake
	for(int i=0;i<snake.size();i++)
	{
		move(snake[i].y,snake[i].x);
		addch(partchar);
	}

	move(maxheight-1,0);
	printw("%d",points);

	move(food.y,food.x);
	addch(etel);

	refresh();
}

void snakeclass::choose_theme()
{
	int CurPosSel = 0;
	int tmp = getch();

	move(9, 36);
	printw("Theme 1");

	move(10, 36);
	printw("Theme 2");

	move(11, 36);
	printw("Theme 3");

	move(12, 36);
	printw("Theme 4");

	move(9, 34);
	addch(SelChar);

	// If input != enter => continue
	while (tmp != '\n')
	{
		tmp = getch();

		// Change select cursor position
		switch(tmp)
		{
			case KEY_DOWN:
				if ( CurPosSel < 3 )
					CurPosSel += 1;
				break;
			case KEY_UP:
				if ( CurPosSel > 0 )
					CurPosSel -= 1;
				break;
			default:
				break;
		}

		// Clear SelChar
		for ( int i=0; i < 4; ++i )
		{
			move(9+i, 34);
			printw(" ");
		}

		switch(CurPosSel)
		{
			// Theme 1
			case 0:
				move(9, 34);
				addch(SelChar);
				theme_id = 0;
				break;

			// Theme 2
			case 1:
				move(10, 34);
				addch(SelChar);
				theme_id = 1;
				break;

			// Theme 3
			case 2:
				move(11, 34);
				addch(SelChar);
				theme_id = 2;
				break;

			// Theme 4
			case 3:
				move(12, 34);
				addch(SelChar);
				theme_id = 3;
				break;

			// Default
			default:
				move(9, 34);
				addch(SelChar);
				theme_id = 0;
				break;
		}

		// For debugging
		// move(2, 1);
		// printw("                  	");
		// move(1, 1);
		// printw("Pos: %d", CurPosSel);

		refresh();
	}

	erase();
	draw_outline();
}

void snakeclass::choose_speed()
{
	int CurPosSel = 0;
	int tmp = getch();

	move(9, 36);
	printw("Low Speed");

	move(10, 36);
	printw("Medium Speed");

	move(11, 36);
	printw("Fast Speed");

	move(12, 36);
	printw("Back");

	move(9, 34);
	addch(SelChar);

	// If input != enter => continue
	while (tmp != '\n')
	{
		tmp = getch();

		// Change select cursor position
		switch(tmp)
		{
			case KEY_DOWN:
				if ( CurPosSel < 3 )
					CurPosSel += 1;
				break;
			case KEY_UP:
				if ( CurPosSel > 0 )
					CurPosSel -= 1;
				break;
			default:
				break;
		}

		// Clear SelChar
		for ( int i=0; i < 4; ++i )
		{
			move(9+i, 34);
			printw(" ");
		}

		switch(CurPosSel)
		{
			// Low Speed
			case 0:
				move(9, 34);
				addch(SelChar);
				speed_level = 0;
				break;

			// Medium Speed
			case 1:
				move(10, 34);
				addch(SelChar);
				speed_level = 1;
				break;

			// High Speed
			case 2:
				move(11, 34);
				addch(SelChar);
				speed_level = 2;
				break;

			// Exit
			case 3:
				move(12, 34);
				addch(SelChar);
				break;

			// Default
			default:
				move(9, 34);
				addch(SelChar);
				speed_level = 0;
				break;
		}

		// DEBUG
		// move(2, 1);
		// printw("                  	");
		// move(1, 1);
		// printw("Pos: %d", CurPosSel);

		refresh();
	}

	erase();
	draw_outline();
}
